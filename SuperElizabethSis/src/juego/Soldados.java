package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Soldado {
	// Variables de instancia
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private Image imagen;
		
	public Soldado (double x, double y, double ancho, double alto) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.imagen = Herramientas.cargarImagen("soldado.gif");
	}
	
	public void dibujarSoldado(Entorno entorno) {
		//entorno.dibujarRectangulo(x, y, this.ancho, this.alto,  0, Color.GREEN);
		entorno.dibujarImagen(imagen, this.x, this.y, 0);
	}
	

	public void moverIzquierda() {
		this.x -= 1.7;
		}
	
	
	public boolean colisionLat(Entorno e) {
		return x <= -50;
	}

	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}
	
	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

}
