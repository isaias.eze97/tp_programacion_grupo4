package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Princesa {
	
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private boolean choco;
	private boolean retroceder;
	private boolean saltar;
	private boolean disparar;
	
	private Image imagen;
	private Image imagen2;
	private Image imagen3;
	private Image imagen4;
	private double velocidad;
	
	Princesa(double x, double y, double velocidad,double alto,double ancho){
		this.x=x;
		this.y=y;
		this.alto=alto;
		this.ancho=ancho;
		this.velocidad=velocidad;
		this.choco=false;
		this.retroceder=false;
		this.saltar=false;
		this.disparar=false;
		
		
		
		imagen4=Herramientas.cargarImagen("princesachoque.png");
		imagen3=Herramientas.cargarImagen("princesasalto.png");
		imagen2=Herramientas.cargarImagen("princesaretroceso.png");
		imagen=Herramientas.cargarImagen("princesa.png");
		
	}
	
	
    void Dibujar(Entorno e){
    	
		// Dibuja el salto 
		 if(saltar==true && retroceder==false && this.y<500 ) {
			 
			 e.dibujarImagen(imagen3,this.x, this.y, 0,2);
		 }
    		
    	 // Dibuja el retroceso
    	 if(retroceder==true && saltar==false) {
    		 
    		 e.dibujarImagen(imagen2,this.x, this.y, 0,2);
    		 
    	 }
    	
    	 //Dibuja a la princesa cuando avanza y cuando est quieta
    	 if( ( retroceder==false  && saltar==false && choco==false ) || (this.y==500 && saltar==true )  ) {
    		 
    	 e.dibujarImagen(imagen,this.x, this.y, 0,2);
    	 }
    	 
    	// Dibuja a la princesa cuando choca con obstaculos
    	 if(choco && retroceder==false  && saltar==false) {
    		 
    		 e.dibujarImagen(imagen4,this.x, this.y, 0,2); 
    	 }
    	 
	}
    
    public Fuego Disparar() {
    	
    	return  new Fuego(this.x, this.y, 8,10,5);
    }
    	
	public void avanzar() {
		
		if(this.y == 500 && this.x < 790)
		this.x+=velocidad;
	}
	
	public void retroceder() {
		
		if(this.y == 500) {
			
		this.x-=velocidad;
		}
		
		if(this.x <= 20) {
			this.x=20;
		}
		
	}
	

	 public void saltar() {
			
			this.y-=145;
			if(this.y < 355) {
				this.y = 500;	
			}
			
		}
		
		  public void caer() {
			
			this.y += 2.5;
			
			 if(this.y >= 500) {
				this.y = 500;
			}
			
		}
	
	
	
	//GETTERS Y SETTERS
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return this.y;
	}
	
	public double getAncho() {
		return this.ancho;
	}
	
	public double getAlto() {
		return this.alto;
	}

	public void Salta(boolean s) {
		this.saltar=s;
	}
	
	public void Retrocede(boolean r) {
		this.retroceder=r;
	}
	
	public void Dispara(boolean d) {
		this.retroceder=d;
	}
	public void Choco(boolean c) {
		this.choco=c;
	}
	
	

	

}
