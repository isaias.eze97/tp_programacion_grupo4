package juego;

import javax.sound.sampled.Clip;
import entorno.Herramientas;


public class Sonidos {
private static Clip disparo,saltar,musica;
	
	public static void iniciojuego() {
		musica = Herramientas.cargarSonido("musica-fondo.wav");
		musica.start();
		}
	
	public static void saltar() {
		saltar= Herramientas.cargarSonido("salto.wav");
		saltar.start();
	}
	
	public static void fuego () {
		disparo = Herramientas.cargarSonido("fuego.wav");
		disparo.start();
	}

}

