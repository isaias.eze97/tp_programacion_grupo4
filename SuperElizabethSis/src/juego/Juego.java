package juego;


import java.awt.Image;
import java.awt.Color;
import java.awt.Font;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	
    private Princesa elisabeth;
	private Fuego[] fuegos;
	private Obstaculo[] obstaculos;
	private Soldado[] soldados;
	private Image fondo;
	private Image gameOver;
	private Image Win;
	private int vidas;
	private int puntos;
	
	
	// Variables y métodos propios de cada grupo
	// ...
	
	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo 4 - v1", 800, 600);
		
		// Inicializar lo que haga falta para el juego
		// ...
		fondo = Herramientas.cargarImagen("fondo.png");
		gameOver= Herramientas.cargarImagen("gameOver.png");
		Win= Herramientas.cargarImagen("Ganador.png");
		
		this.elisabeth= new Princesa(50,500,3,50,50);
		this.fuegos= new Fuego[100];
		
		this.soldados = new Soldado[3];
		this.soldados[0] = new Soldado(Math.random()*100+900, 500, 50, 50);
		this.soldados[1] = new Soldado(Math.random()*100+1600, 500, 50, 50);
		this.soldados[2] = new Soldado(Math.random()*100+2800, 500, 50, 50);
		
		this.obstaculos = new Obstaculo[5];
		this.obstaculos[0] = new Obstaculo( 550,  480, 50, 50, 1.5);
		this.obstaculos[1] = new Obstaculo( 850,  480, 50, 50, 1.5);
		this.obstaculos[2] = new Obstaculo( 1100, 480, 50, 50, 1.5);
		this.obstaculos[3] = new Obstaculo( 1450, 480, 50, 50, 1.5);
		this.obstaculos[4] = new Obstaculo( 1750, 480, 50, 50, 1.5);
		
		this.vidas=3;
		this.puntos=0;
		
		// Inicia el juego!
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick()
	{
		// Procesamiento de un instante de tiempo
		// ...
		
		//Dibuja el fondo
		entorno.dibujarImagen(fondo, 400, 300, 0, 0.62);
		
		//Dibuja a la princesa
		elisabeth.Dibujar(entorno);
		
		//Avance de la princesa
		if(entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			elisabeth.Retrocede(false);
		    elisabeth.Salta(false);
		    elisabeth.Choco(false);
		    
			elisabeth.avanzar();
		}
		
		//Retroceso de la princesa
		if(entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			elisabeth.Retrocede(true);
		    elisabeth.Salta(false);
		    
			elisabeth.retroceder();
		}
		
		//Salto de la princesa
		if(entorno.sePresiono(entorno.TECLA_ARRIBA)) {
			Sonidos.saltar();
			elisabeth.saltar();
			elisabeth.Retrocede(false);
			} else {
			elisabeth.caer();
			elisabeth.Salta(false);
			elisabeth.Retrocede(false);
		}
		
		for(int i = 0; i < fuegos.length;  i++) {
			
			if (fuegos[i] != null) {
				fuegos[i].Dibujar(entorno);
				fuegos[i].Mover();	
			}
		}

		
		
		if(entorno.sePresiono(entorno.TECLA_ESPACIO)){
		    Sonidos.fuego();
			for (int i = 0; i < fuegos.length; i++){
				if(fuegos[i] == null){
					fuegos[i] = elisabeth.Disparar();
					break;
					}
			}
		   
		}
		
		if(colisionPrincesaConObstaculos(obstaculos)) {
			elisabeth.Choco(true);
			elisabeth.Retrocede(false);
		    elisabeth.Salta(false);
		    if(vidas>0 && puntos<65) {
		    vidas-=1;
		    }
	    }
	    
		if (colisionPrincesaConSoldados(soldados)) {
			elisabeth.Choco(true);
			elisabeth.Retrocede(false);
		    elisabeth.Salta(false);
	    	if(vidas>0 && puntos<65) {
	    	vidas-=1;
	    	}
	    }
		
					
		//Dibujar soldados
	   
		for (int i = 0; i < this.soldados.length; i++) {
			
			if(soldados[i] != null) {
				
				this.soldados[i].dibujarSoldado(entorno);
				soldados[i].moverIzquierda();
				} else {
					this.soldados[i] = new Soldado(800+Math.random()+700, 500, 50, 50);
				}
		}
		
		// Dibujar obstaculos
		for (int i = 0; i < this.obstaculos.length; i++) {
			this.obstaculos[i].dibujarse(entorno);	
		}
		
		
		colisionSoldadoConFuego();
		MovObstaculos();
		MovSoldados();
		GameOver();
		Win();
		
        entorno.cambiarFont(Font.DIALOG, 20, Color.black);
		
		if(vidas > 0) {
		entorno.escribirTexto("VIDAS: " + vidas, 10, 30);
		}
		
		entorno.escribirTexto("PUNTOS: "+ puntos , 680, 30);
		
		if(entorno.sePresiono(entorno.TECLA_ENTER))
		{
			Juego juego = new Juego();
		}
		
	}
	
	
	
	//Funcion cuando la Princesa pierde
	
	public void GameOver() {
		if(vidas <= 0) {
			
			entorno.dibujarImagen(gameOver, 400, 300, 0, 3.6);
			
			entorno.cambiarFont("Times New Roman", 30, Color.red);
			entorno.escribirTexto("Tu puntuación es de: " + puntos, 225, 530);
			entorno.escribirTexto("Presioná ENTER para volver a comenzar.", 220, 580);
		}
	}
	
	//Funcin que se ejecuta cuando gana la princesa
	public void Win() {
		if(puntos==65) {


			entorno.dibujarImagen(Win, 400, 300, 0, 1.25);
			entorno.cambiarFont("Times New Roman", 30, Color.pink);
			entorno.escribirTexto("Presion ENTER para volver a comenzar!", 145, 50);
				
		}
			
	}
	
	
	//Funcion Movimiento Soldados
	public void MovSoldados() {
		for (int i = 0; i < soldados.length; i++) {
			if (soldados[i] != null) {
			if(soldados[i].colisionLat(entorno)){
				this.soldados[i] = null;
				this.soldados[i] = new Soldado(800+Math.random()+700, 500, 50, 50);
				}
			}
		}
	}
	
	
	
	//Funcion Movimiento Obstaculo
	public void MovObstaculos() {
		for (int i = 0; i < obstaculos.length; i++) {
			obstaculos[i].moverIzquierda();
			if (obstaculos[i].colisionConLat(entorno)) {
				obstaculos[i].setX(850);
			}
		}
	}
	
	
	
	//Colisin princesa con obstaculos
	public boolean colisionPrincesaConObstaculos( Obstaculo[] obstaculo) {
	for(int i = 0; i < obstaculo.length; i++){
		if (obstaculo[i]!= null) {
			if(elisabeth.getX() >= obstaculo[i].getX()-obstaculo[i].getAncho()/2 && elisabeth.getX()<=obstaculo[i].getX()-24 && elisabeth.getY() >= obstaculo[i].getY()-obstaculo[i].getAlto()/2 && elisabeth.getY()<=obstaculo[i].getY()+obstaculo[i].getAlto()/2) {
				return true;
			}
		} else {
			this.obstaculos[i] = new Obstaculo(1000, 500, 50, 50, 1.5);
		}
	 }
	return false;
	}
	
	
	//Colision princesa con obstaculos
	public boolean colisionPrincesaConSoldados( Soldado[] soldado) {
		for(int i = 0; i < soldado.length; i++){
			if (soldado[i]!= null) {
				if(elisabeth.getX() >=soldado[i].getX()-soldado[i].getAncho()/2 && elisabeth.getX() <= soldado[i].getX()-21 && elisabeth.getY()>=soldado[i].getY()-soldado[i].getAlto()/2 && elisabeth.getY()<=soldado[i].getY()+soldado[i].getAlto()/2) {
					this.soldados[i] = null;
					return true;
				}
			}
		 }
		return false;
	}
	
	
	//Colisin Soldado con Fuego
	public void colisionSoldadoConFuego() {
		
		for(int i=0; i< fuegos.length;i++) {
			for(int j=0; j< soldados.length;j++) {
				
				if(fuegos[i]!=null && soldados[j]!=null) {
					
					if(fuegos[i].getX() >= soldados[j].getX()-soldados[j].getAncho()/2 && fuegos[i].getX() <=soldados[j].getX()+soldados[j].getAncho()/2 && fuegos[i].getY()>=soldados[j].getY()-soldados[j].getAlto()/2 && fuegos[i].getY()<=soldados[j].getY()+soldados[j].getAlto()/2) {
						fuegos[i]=null;
						soldados[j]=null;
						puntos+=5;
					}
				} 
			}
		}
	}
	

	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
		if (juego != null) {
			Sonidos.iniciojuego();
			}
	}
}
