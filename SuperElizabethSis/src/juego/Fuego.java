package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Fuego {
	
	private double x;
	private double y;
	private double alto;
	private double ancho; 
	private double velocidad;
	private Image imagen;
	
    
    
   
    
   Fuego(double x, double y, double velocidad, double alto, double ancho){
	   
    	this.x=x;
    	this.y=y;
    	this.alto=alto;
    	this.ancho=ancho;
    	this.velocidad=velocidad;
    	imagen=Herramientas.cargarImagen("fuego1.png");
    }
    
    public void Dibujar(Entorno e) {
		  
		  
		  e.dibujarImagen(imagen,this.x, this.y,3.5 ,1);
		  
		 
			  		   
		}
    
   public void Mover() {
	   this.x+=velocidad;
   }
   
   
    
	public double getX() {
		return x;
	}


	public double getY() {
		return y;
	}
   
	public double getAncho() {
		return this.ancho;
	}
	
	public double getAlto() {
		return this.alto;
	}

}


