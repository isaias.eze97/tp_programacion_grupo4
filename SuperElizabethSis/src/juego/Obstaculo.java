package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {
	//Variables de Instancia
	
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private double velocidad;
	private Image imagen;
	
	//Constructor
	Obstaculo(double x, double y, double ancho, double alto, double velocidad) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.velocidad = velocidad;
		this.imagen = Herramientas.cargarImagen("Obstaculo.png");
	}
	
	public void moverIzquierda() {
		this.x = this.x - velocidad;
		}
	
	public void dibujarse(Entorno entorno) {
		//Dibuja Obstaculo
		//entorno.dibujarImagen(imagen, this.x, this.y, 0);
		entorno.dibujarImagen(imagen, this.x,this.y, 0, 1);
	}
	
	public boolean colisionConLat(Entorno e) {
		return x <= -50;
	}

	public double getX() {
		return x;
	}


	public double getY() {
		return y;
	}


	public double getAncho() {
		return ancho;
	}

	
	public double getAlto() {
		return alto;
	}

}
